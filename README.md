CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Provides an integration with PayEx payment gateway, when using Payment.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/payment_payex

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/payment_payex


REQUIREMENTS
------------

This module requires following modules outside of Drupal core:

 * PayEx - https://www.drupal.org/project/payex
 * Payment - https://www.drupal.org/project/payment


INSTALLATION
------------

 * Install the Payment PayEx module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module and its
       dependencies.
    2. Navigate to Administration > Configuration > Web Services > PayEx to
       manage and configure settings.
    3. Add a new PayEx setting and configure as desired. Save.
    4. Navigate to Administration > Configuration > Web Services > Payment >
       Payment methods to enable payment methods.


MAINTAINERS
-----------

 * Igor Kvyatkovskyi (unrealauk) - https://www.drupal.org/u/unrealauk

Supporting organizations:

 * AnyforSoft - https://www.drupal.org/anyforsoft
 * WAPPO - https://www.drupal.org/wappo
 * Drupal Ukraine Community - https://www.drupal.org/drupal-ukraine-community
