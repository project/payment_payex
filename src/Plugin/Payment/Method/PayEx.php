<?php

namespace Drupal\payment_payex\Plugin\Payment\Method;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;
use Drupal\payex\Entity\PayExSetting;
use Drupal\payex\Service\PayExApiFactory;
use Drupal\payment\EventDispatcherInterface;
use Drupal\payment\Plugin\Payment\Method\Basic;
use Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A PayEx payment method.
 *
 * @PaymentMethod(
 *   label = "PayEx",
 *   deriver = "\Drupal\payment_payex\Plugin\Payment\Method\PayExDeriver",
 *   id = "payment_payex",
 *   operations_provider =
 *   "\Drupal\payment_payex\Plugin\Payment\Method\PayExOperationsProvider",
 * )
 */
class PayEx extends Basic {

  /**
   * Factory for creating instances of PayExApi.
   *
   * @var \Drupal\payex\Service\PayExApiFactory
   */
  protected $payExApiFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct($configuration, string $plugin_id, $plugin_definition, ModuleHandlerInterface $module_handler, EventDispatcherInterface $event_dispatcher, Token $token, PaymentStatusManagerInterface $payment_status_manager, PayExApiFactory $payExApiFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $module_handler, $event_dispatcher, $token, $payment_status_manager);
    $this->payExApiFactory = $payExApiFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('payment.event_dispatcher'),
      $container->get('token'),
      $container->get('plugin.manager.payment.status'),
      $container->get('payex.api_factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function doExecutePayment() {
    $configuration = $this->getPluginDefinition();

    $totalAmount = 0;
    foreach ($this->getPayment()->getLineItems() as $line_item) {
      $totalAmount += $line_item->getTotalAmount();
    }
    $totalAmount .= '00';

    $payex_setting_id = $this->getPayment()
      ->getPaymentMethod()
      ->getPluginDefinition()['payex_setting'] ?? NULL;

    $payex_setting = PayExSetting::load($payex_setting_id);

    /** @var \Drupal\payex\Service\PayExApi $api */
    $api = $this->payExApiFactory->get($payex_setting_id);

    $return_url = Url::fromRoute('payment_payex.complete', [
      'uuid' => $this->getPayment()->UUID(),
    ], [
      'absolute' => TRUE,
    ])->toString();
    $params = [
      'purchaseOperation' => $api->getPurchaseOperation(),
      'price' => (string) $totalAmount,
      'priceArgList' => '',
      'orderID' => $this->getPayment()->id(),
      'productNumber' => !empty($line_item) ? $line_item->getName() : '',
      'description' => !empty($line_item) ? $line_item->getDescription() : '',
      'clientIPAddress' => filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_VALIDATE_IP),
      'clientIdentifier' => "USERAGENT=" . filter_input(INPUT_SERVER, 'HTTP_USER_AGENT', FILTER_SANITIZE_SPECIAL_CHARS),
      'clientLanguage' => $api->getPayExLanguage(),
      'returnUrl' => $return_url,
      'view' => $configuration['payment_method'],
      'additionalValues' => "RESPONSIVE=1",
      'currency' => $payex_setting->get('defaultCurrencyCode'),
    ];

    $response = $api->initialize($params);

    if (!empty($response) && $response['status']['errorCode'] == 'OK' && !empty($response['redirectUrl'])) {
      $url = Url::fromUri($response['redirectUrl']);
      $response = new TrustedRedirectResponse($url->toString());
      $response->addCacheableDependency($url);
      $response->send();
    }
    else {
      drupal_set_message(t('Payment error, the transaction could not be initialized. Please, try again later.'), 'error');
    }
  }

}
