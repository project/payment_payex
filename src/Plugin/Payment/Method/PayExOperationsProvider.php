<?php

namespace Drupal\payment_payex\Plugin\Payment\Method;

use Drupal\payment\Plugin\Payment\Method\PaymentMethodConfigurationOperationsProvider;

/**
 * Provides payment_basic operations based on config entities.
 */
class PayExOperationsProvider extends PaymentMethodConfigurationOperationsProvider {

  /**
   * {@inheritdoc}
   */
  protected function getPaymentMethodConfiguration($plugin_id) {
    $entity_id = substr($plugin_id, 14);

    return $this->paymentMethodConfigurationStorage->load($entity_id);
  }

}
