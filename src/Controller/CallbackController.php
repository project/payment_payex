<?php

namespace Drupal\payment_payex\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Url;
use Drupal\payex\Service\PayExApiFactory;
use Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class CallbackController.
 *
 * @package Drupal\payment_payex\Controller
 */
class CallbackController extends ControllerBase {

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;


  /**
   * The payment status plugin manager.
   *
   * @var \Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface
   */
  protected $paymentStatusManager;

  /**
   * The site config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $siteConfig;

  /**
   * Factory for creating instances of PayExApi.
   *
   * @var \Drupal\payex\Service\PayExApiFactory
   */
  protected $payExApiFactory;

  /**
   * CallbackController constructor.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   * @param \Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface $payment_status_manager
   *   The payment status manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\payex\Service\PayExApiFactory $payExApiFactory
   *   The factory for PayExApi objects.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, PaymentStatusManagerInterface $payment_status_manager, ConfigFactoryInterface $config_factory, PayExApiFactory $payExApiFactory) {
    $this->entityRepository = $entity_repository;
    $this->paymentStatusManager = $payment_status_manager;
    $this->siteConfig = $config_factory->get('system.site');
    $this->payExApiFactory = $payExApiFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('plugin.manager.payment.status'),
      $container->get('config.factory'),
      $container->get('payex.api_factory')
    );
  }

  /**
   * Handler when payment is complete.
   *
   * @param string $uuid
   *   Payment UUID.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirects back to the different result pages.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function complete($uuid) {
    $current_status = '';

    /** @var \Drupal\Payment\Entity\Payment $payment */
    if ($payment = $this->entityRepository->loadEntityByUuid('payment', $uuid)) {
      $current_status = $payment->getPaymentStatus()->getpluginId();
    }

    $payex_setting_id = $payment->getPaymentMethod()
      ->getPluginDefinition()['payex_setting'] ?? NULL;

    $completeParams = [
      'orderRef' => stripcslashes($_GET['orderRef']),
    ];

    /** @var \Drupal\payex\Service\PayExApi $api */
    $api = $this->payExApiFactory->get($payex_setting_id);

    $result = $api->complete($completeParams);

    if ($payment && $current_status == "payment_success") {
      $response = new RedirectResponse(Url::fromRoute('payment_payex.successful')
        ->toString());
    }
    else {
      $response = new RedirectResponse(Url::fromRoute('payment_payex.failed', ['uuid' => $uuid])
        ->toString());
      if ($result['status']['errorCode'] == 'OK') {
        if ($result['transactionStatus'] == 0 || $result['transactionStatus'] == 3) {
          $payment->setPaymentStatus($this->paymentStatusManager->createInstance('payment_success'));
          $response = new RedirectResponse(Url::fromRoute('payment_payex.successful')
            ->toString());
        }
        elseif ($result['transactionStatus'] == 5) {
          $response = new RedirectResponse(Url::fromRoute('payment_payex.cancel', ['uuid' => $uuid])
            ->toString());
          $payment->setPaymentStatus($this->paymentStatusManager->createInstance('payment_cancelled'));
        }
        else {
          $payment->setPaymentStatus($this->paymentStatusManager->createInstance('payment_failed'));
        }
        $payment->save();
      }
      else {
        drupal_set_message($this->t('Payment error, the transaction could not be verified.'));
      }
    }
    return $response;
  }

  /**
   * Handler when payment has error.
   *
   * @param string $uuid
   *   Payment UUID.
   *
   * @return array
   *   A render array for the error page.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function error($uuid) {
    /** @var \Drupal\Payment\Entity\Payment $payment */
    $payment = $this->entityRepository->loadEntityByUuid('payment', $uuid);
    $payment->setPaymentStatus($this->paymentStatusManager->createInstance('payment_failed'));
    $payment->save();

    return [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t('Your payment failed. Please try again or contact @site if the problem persists.', ['@site' => $this->siteConfig->get('site_mail')]) . '</p>',
    ];
  }

  /**
   * Handler when payment was cancel.
   *
   * @param string $uuid
   *   Payment UUID.
   *
   * @return array
   *   A render array for the cancel page.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function cancel($uuid) {
    /** @var \Drupal\Payment\Entity\Payment $payment */
    $payment = $this->entityRepository->loadEntityByUuid('payment', $uuid);
    $payment->setPaymentStatus($this->paymentStatusManager->createInstance('payment_cancelled'));
    $payment->save();

    return [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t('Your payment was cancelled.') . '</p>',
    ];
  }

  /**
   * Handler when payment is successfully.
   *
   * @return array
   *   A render array for the successful page.
   */
  public function successful() {
    return [
      '#title' => $this->t('Payment complete'),
      '#type' => 'markup',
      '#markup' => "<p>" . $this->t('Your PayEx payment was completed successfully. Thanks!') . "</p>",
    ];
  }

}
