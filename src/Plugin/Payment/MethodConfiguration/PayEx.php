<?php

namespace Drupal\payment_payex\Plugin\Payment\MethodConfiguration;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\payment\Plugin\Payment\MethodConfiguration\Basic;
use Drupal\Component\Utility\NestedArray;

/**
 * Provides the configuration for the payment_payex payment method plugin.
 *
 * @PaymentMethodConfiguration(
 *   description = @Translation("A PayEx payment method."),
 *   id = "payment_payex",
 *   label = @Translation("PayEx")
 * )
 */
class PayEx extends Basic {

  /**
   * {@inheritdoc}
   */
  public function processBuildConfigurationForm(array &$element, FormStateInterface $form_state, array &$form) {

    $element['payex'] = [
      '#type' => 'container',
      '#weight' => -99,
    ];

    // For initial available only these.
    $payment_method_options = [
      'CREDITCARD' => t('Credit Card'),
      'DIRECTDEBIT' => t('Direct Bank Debit'),
    ];

    $element['payex']['payment_method'] = [
      '#type' => 'select',
      '#title' => $this->t('Payment method'),
      '#options' => $payment_method_options,
      '#default_value' => $this->configuration['payment_method'] ?? '',
      '#required' => TRUE,
    ];

    $payex_settings = \Drupal::service('entity_type.manager')
      ->getStorage('payex_setting')
      ->loadMultiple();

    $payex_setting_options = [];

    /* @var \Drupal\payex\Entity\PayExSetting $payexSetting */
    foreach ($payex_settings as $key => $payexSetting) {
      $payex_setting_options[$key] = $payexSetting->get('name');
    }

    $element['payex']['payex_setting'] = [
      '#type' => 'select',
      '#title' => $this->t('PayEx settings'),
      '#options' => $payex_setting_options,
      '#default_value' => $this->configuration['payex_setting'] ?? '',
      '#required' => TRUE,
      '#description' => $this->t('You can add the new settings <a href=":url">here</a>.', [':url' => Url::fromRoute('payex.admin_page')->toString()]),
    ];

    parent::processBuildConfigurationForm($element, $form_state, $form);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $parents = $form['plugin_form']['payex']['#parents'];
    array_pop($parents);
    $values = $form_state->getValues();
    $values = NestedArray::getValue($values, $parents);

    $this->configuration['payment_method'] = $values['payex']['payment_method'];
    $this->configuration['payex_setting'] = $values['payex']['payex_setting'];
  }

  /**
   * Added additional settings.
   *
   * @return array
   *   Configs.
   */
  public function getDerivativeConfiguration() {
    return [
      'payment_method' => $this->configuration['payment_method'] ?? '',
      'payex_setting' => $this->configuration['payex_setting'] ?? '',
    ];
  }

}
